# Urban Owl

Urban Owl is a dark VS Code theme based off a Monokai theme that has been modified with some color changes and support targeted towards Rust and rust-analyzer.

![screenshot](screenshot.png "screenshot")

## Bracket Pair Colorizer 2

Add the following to your JSON settings to add support for [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2).

```json
{
    "bracket-pair-colorizer-2.colors": [
        "#e6db74",
        "#d3afff",
        "#7fa5ff",
        "#bb9696"
    ],
    "bracket-pair-colorizer-2.colorMode": "Independent",
}
```
